import json
import sys
import re

#different combinations of parameters give different summarizations

#some modifications required:
#listen to punctuation as a way to cut segments, treat paragraph as ending with punctuation
#by using the new parser we also need to process areas of text and join them with spaces
#experiment with various values of countr

#sometimes i think we can provide a title/uri region that is separate to the actual semantic region
#just to flesh it all out
#but pretty much the two types of semantics : consensus and rarity, plus the title/uri stuff gives awesomeness
#as well as that what we can do is find an awesome robust html parser in python or make one


#i like the current version for some things, it is useful at finding names viz:Natan.Joi.Bluefin
#but it also sucks at some other sites
#it would be cool to give people a list of options
#like search results
#and let them choose
#and then append the results
#so i think I shold add the below in as a module, not as a replacement

#also use the meta info on a page
#combine this with scaling based on title and uri
#and we have for sure semantic relevance
#also combine (later) with a domain index that removes boilerplate and we have awesomness
#also can tune
###
#can scale based on distance from start of file
#should filter out low freq words from each seg
#and also we should first score locally high freq words (like China) higher so segs with these get pushed up
#first i will try neg scaling by length
#neg scaling by length does not work

#notes it works well on cross-corpus tests
#like news items with the big corpus, not the news corpus
#also there is a way to make it much better
#instead of only greedy forward
#we need a way to do greedy forward and greed back
#like you have to expand around rare words
#expand forward and back around signal rare words and look for regions like that
#add a way to make it more adaptive
#like for ever text it reads it also adds to its frequency table

print "Opening freq data file "+ sys.argv[1]
fi = open(sys.argv[1],'r')
print "Loading JSON data"
d = json.load(fi)
print "Making into a dictionary for fast access"
#d = {}

#for litem in l:
#	d[litem[0]] = litem[1]

#now we have the dict we can use it

defth = 1.5
btable = {}
gfreq = {}
while True:
	btable = {}
	gfreq = {}
	print "\nEnter mode (Rate,Weight,Delta Rate,Delta Weight):"
	md = sys.stdin.readline().strip()
	if 'q' in md:
		break
	print "\nEnter threshold : "
	th = sys.stdin.readline()
	if 'q' in th:
		break
	try:
		threshold = float(th)
	except:
		print "Defaulting to " + `defth`
		threshold = defth
	print "\nEnter test file : "
	inp = sys.stdin.readline()
	if 'quit()' in inp:
		break
	src = open(inp.rstrip(),'r')
	def segsrc(src):
		src = src.read()
		words = re.findall(r"[\w-]+|[^\w\s]|\s+", src, re.UNICODE)
		prev_countr = 0.0
		countr = 0.0
		rate = 0.0
		lrate = 0.0
		lval = 1.0
		val = 1.0
		prev_rate = 0.0
		prev_seg = []
		seg = []
		jug = []
		ws = len(words)
		i = 0
		for word in words:
			i += 1
			scale = (ws-i)*1.0/ws
			val = 2.0
			word = word.replace("\n"," ").replace("\r"," ")
			if len(word) == 0:
				continue
			word = word.strip()
			if len(word) < 2 and not word.isalpha():
				continue	
			if word in d:
				val = d[word]
				if word.istitle():
					val *= 10
			elif '-' in word:
				tw = word.partition('-')
				if tw[0] in d:
					val = d[tw[0]]
				else:
					val = 2.0
				if tw[2] in d:
					val += d[tw[2]]
				else:
					val += 2.0
			if word.isupper() and word.lower() in d:
				val *= d[word.lower()]*3
			if word in gfreq:
				gfreq[word][0] += 1
			else:
				gfreq[word] = [1,scale]
			#try scaling val by scale
			#val *= scale
			#try scaling gfreq[word] by scale
			rate = countr/(len(seg)+1)
			deltarate = abs(rate - lrate)
			lrate = rate
			deltaweight = 1.0*lval/val if lval > val else 1.0*val/lval
			lval = val
			ljust = " "*(30-len(word))
			#sys.stdout.write( ''.join([word,ljust," = "]))
			"""
			if md == 'R':
				print `rate`
			elif md == 'W':
				print `countr`
			elif md == 'DR':
				print `deltarate`
			elif md == 'DW':
				print `deltaweight`
			"""
			if md == 'R' and rate > threshold or \
			   md == 'W' and countr > threshold or \
			   md == 'DR' and deltarate > threshold or \
			   md == 'DW' and deltaweight > threshold: #something happens that makes us finalise this arragement
				#if len(seg) == 0:
				#	continue
				dond = 0
			        if len(word)== 1:
					if not word[0].isalpha():
						#seg.append(word)
						dond = 1
				#sys.stdout.write("\n " + `deltaweight`)
				jseg = ' '.join(seg)
				if len(seg) > 0:
					if jseg in btable:
						btable[jseg][0] += countr
					else:
						btable[jseg] = [countr,seg]
				#sys.stdout.write("\n"+' '.join(seg))
				#jj = sys.stdin.readline()
				#if 'q' in jj:
				#	break
				prev_countr = countr
				prev_seg = seg
				prev_rate = prev_countr/(1+len(prev_seg))
				if dond == 0:
					countr = val
					seg = [word]
				else:
					countr = 0.0
					seg = []
				jug = []
			else:
				countr += val
				seg.append(word)
		jseg = ' '.join(seg)

		if len(seg) > 0:
			if jseg in btable:
				btable[jseg][0] += countr
			else:
				btable[jseg] = [countr,seg]
		#sys.stdout.write("\n")
		#sys.stdout.write(''.join(seg))

	segsrc(src)
	#for ji in gfreq.iteritems():
	#	gfreq[ji[0]] = 1.0/ji[1]
	jframe = {}
	for lolo in btable.iteritems():
		bt = lolo[1][1]
		jzaz = 0
		for word in bt:
			val = 1.0
			if word in d:
				val = d[word]
				if word.istitle():
					val = 10 *d[word]
			elif '-' in word:
				tw = word.partition('-')
				if tw[0] in d:
					val = d[tw[0]]
				else:
					val = 2.0
				if tw[2] in d:
					val += d[tw[2]]
				else:
					val += 2.0
			if word.isupper() and word.lower() in d:
				val *= d[word.lower()]*3
			gf = gfreq[word][0]*gfreq[word][0]*gfreq[word][1]*gfreq[word][1]
			jzaz += val*gf
		jzaz /= len(bt)
		lolo[1][0] += jzaz
		#if len(bt) > 1 and lolo[0].istitle(): # could scale this 10 by the size of the rest of the doc
		#	lolo[1][0] *= 10
		jframe[lolo[0]] = lolo[1]
	
	
	bs = sorted(jframe.iteritems(),key=lambda (k,v):v,reverse=True)
	for i in xrange(10):
		sys.stdout.write(bs[i][0] + " " + `bs[i][1]` + "\n")

