Bird flu research to be published in full

22:20 17 February 2012 by Debora MacKenzie
For similar stories, visit the Epidemics and Pandemics , Bird Flu and Genetics Topic Guides
Two studies reporting what makes the H5N1 bird flu virus transmissible in mammals should be published fully, with no details left out, say flu experts meeting at the World Health Organisation in Geneva.

They say the benefits to public health outweigh the risks of bioterrorism, but they concede that publication should be delayed so scientists can "engage in public communication" aimed at preventing "unnecessary anxiety".

The decision to publish comes just a few short weeks after the National Science Advisory Board for Biosecurity (NSABB), the US's top biosecurity panel, asked these virologists not to publish the research in detail.

The NSABB worried that bioterrorists would use the information to make dangerous, transmissible H5N1 �V or that other, less careful researchers would repeat the work and let the virus get away.

Why the difference? The researchers themselves have always wanted to publish without restriction, and of the 22 people who met in Geneva, fully half were leading flu researchers, and none of the rest were biosecurity experts concerned with bioterrorism.

Lightning rod

But it wasn't just a matter of researchers representing their own interests. Unlike the US panel, the Geneva group included public health experts from Indonesia and Vietnam, where people are dying of H5N1 and the virus is actively evolving. The chair of the censorship-favouring US panel, Paul Keim of Northern Arizona University in Flagstaff, was also part of this agreement, which the WHO describes as unanimous.

The issue has become a lightning rod for debate because the only thing standing between H5N1 bird flu, which infects birds in half a dozen countries in Asia and in Egypt, and a nasty pandemic is that the virus is hard for humans to catch. But we now know it can it become easily transmissible, in mammals anyway. Last year two labs created versions of the flu that transmits fine in ferrets, the best stand-ins for people.

Most worryingly, one of the labs, in Rotterdam, the Netherlands, discovered that as few as five mutations made H5N1 as catchable as ordinary flu, while remaining as lethal as bird flu is now �V and it kills around half the people who get it. This, says Keiji Fukuda, the WHO official who chaired today's meeting in Geneva, "changed our perception of the risk from H5N1".

Need-to-know basis

The two papers were submitted last year to the journals Nature and Science. US authorities �V because the US National Agency for Allergy and Infectious Disease funded the work �V sounded the alarm over fears that the Rotterdam work, especially, would provide a weapon to bioterrorists.

The NSABB met in Washington DC and in December asked that the work not be published in full, keeping the methods used to make the virus, and the mutations that made it nasty, under wraps.

The journals said they would accept this, as long as some mechanism could be found to get that information to people who needed it �V notably scientists handling H5N1 and public health authorities in countries with the virus, both of whom need to watch for those mutations.

Data minders

"Now that we know how easy it is for this virus to mutate [and] to spread in aerosols, there's every reason to suspect that in the natural environment this virus will emerge," says Bruce Alberts, editor of Science, speaking about the Geneva meeting on Friday at the American Association for the Advancement of Science meeting in Vancouver, Canada. "This is a tremendous opportunity to mobilise resources to protect against a pandemic. The information has to be made open so that this research can be done rapidly."

Finding a way to make it available, without publishing openly in the journals, was what the flu researchers had been trying to work out in Geneva over the last two days.

They gave up. There was no way, said Fukuda, to do this in a short time. "Who holds the sensitive information?" he said in a teleconference with reporters on Friday. "Who decides under what conditions to release it?"

In other words, should the US funders of the work decide who gets the withheld details? Or the American and British journal publishers? Must the poorer countries at risk from H5N1 depend on British and American largesse for data vital to protecting themselves, after they supplied the viruses with which the research was done in the first place? The political sensitivity of the issue may have carried more weight in Geneva than it did in Washington.

Weighing the risks

In addition, the researchers discount the possibility of a bioterrorist making the virus. "It's just too difficult for more than a few labs to do it," says Ab Osterhaus, head of the Rotterdam lab.

"What is the chance of a terrorist producing this virus? You have to be a pretty sophisticated scientist," agrees Alberts. The advantages of keeping scientific details from putative bioterrorists, he said, must be weighed against the advantages of working on vaccines, which "can be done more rapidly in an open environment".

Questions of bioterror are one thing. Making sure further work with the virus is done safely is another. The world's top flu labs recently agreed to a voluntary moratorium on the research until late March, while those issues are discussed.

Fukuda says the moratorium will be extended, while the WHO convenes another meeting with biosafety experts to discuss ways to take the research forward safely. "This may be a case where we limit the number of labs working on the virus, so we limit the chance of the virus escaping," says Alberts.

Additional reporting by Rowan Hooper in Vancouver

