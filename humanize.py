from google.appengine.api import urlfetch

#idea do adaptive and see the number of in requests
#so we assign shorter urls to more requested resources (not by click but by in request)
#from google.appengine.ext import webapp
import webapp2
from google.appengine.ext import db
from google.appengine.ext.webapp.util import run_wsgi_app

# Regular expressions used for parsing

def doit(uri,sro=None):
	try:	
		result = urlfetch.fetch(uri,allow_truncated=True)
	except:
		return "!!!!baduri"
	if result.status_code == 200:
		return "ok"
	else:
		return None
		
class Location(db.Model):
	href=db.TextProperty()

class Humanizer(webapp2.RequestHandler):
	def get(self):
		global rsrc
		self.response.headers['Content-Type'] = 'text/html'
		url = self.request.url
		usrasks = url.partition('humanize/')[2]
		src = usrasks.partition('mangy-goats-devouring/')
		srcuri = src[0]
		srcusr = src[2]
		cindex = srcuri.find('c')
		usruri = ''.join([srcuri[:cindex],':',srcuri[cindex+1:]])
		uriresult = doit(usruri) 
		if uriresult == "!!!!baduri":
			self.response.out.write("__bad url! Bad!")
			return		
		if len(srcusr) > 200:
			srcusr = srcusr[:200]	
		lh = Location.get_by_key_name(srcusr)					
		if lh is not None:  
			if lh.href == usruri:
				self.response.out.write(srcusr) 
				return
			else:
				self.response.out.write("already_devouring_goat")
          	else:
			self.response.out.write(srcusr)
			lh = Location(key_name=srcusr)
			lh.href = db.Text(usruri)
			lh.put()
                
class Usher(webapp2.RequestHandler):
	def get(self):
		lh = Location.get_by_key_name(self.request.path.lstrip('/'))
		uagent = self.request.headers['User-Agent']
		if lh is None:
			self.response.out.write('<body>It\'s not your fault but that page does not exist. Yet.</body>')		
			return
		if 'facebook' in uagent:
			self.redirect(str(lh.href))
		else:
			self.response.headers['Content-Type'] = 'text/html'
			lhhref = lh.href
			if lhhref[:4] != 'http':
				lhhref = '://'.join(['http',lhhref])
			self.response.out.write(''.join(['<body><script>\nwindow.location.href="',lhhref,'";\n</script></body>']))

routes = [('/humanize/.*',Humanizer),
	  ('/[\w+\.*]+/?.*',Usher)]

app = webapp2.WSGIApplication(routes=routes)

def main():
	global freq,parser
	if freq is None or len(freq) < 10:	
		fi = open('ff.dict','r')
		freq = json.load(fi)
		fi.close()
	if parser is None:
		parser = _DeHTMLParser()
	run_wsgi_app(application)

if __name__ == "__main__":
	main()

